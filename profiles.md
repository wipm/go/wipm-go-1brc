# Profiles

Documents what was done for each profilei, note there are some differences to the stated challenge:

:tag:v2.1 parallel parsing   |  24401575657 ns/op      23521509560 B/op  1000051740 allocs/op  25.845s  (8 workers)
:tag:v2.0 chunk reads        | 181718007203 ns/op      23506915592 B/op  1000050465 allocs/op 181.857s
:tag:v1 basic implementation | 259803323898 ns/op      48003246472 B/op  2000050885 allocs/op 260.013s


## tag:v2.1 parallel parsing

Uses chunk read approach for reading the file but has multiple parsers working in parallel.  Each parser keeps a tally of the stats it has loaded and once all chunks have be parsed passes them to an aggregator that will combines the stats

Things to consider from this:

- There are more allocations taking place as each parser maintains its own stats
- The parsing routine still looks right for optimization


:stats

BenchmarkSolve-16              1        24401575657 ns/op       23521509560 B/op        1000051740 allocs/op
ok      wipm/brc/read   25.845s


::cpu profile inspection

Showing top 10 nodes out of 34
      flat  flat%   sum%        cum   cum%
    39.36s 20.45% 20.45%    189.14s 98.25%  wipm/brc/read.parseChunk
    32.04s 16.64% 37.09%     60.54s 31.45%  runtime.mapaccess2_faststr
    31.94s 16.59% 53.68%     31.94s 16.59%  strconv.readFloat
    13.09s  6.80% 60.48%     13.09s  6.80%  memeqbody
    12.23s  6.35% 66.84%     20.88s 10.85%  runtime.mallocgc
    10.79s  5.61% 72.44%     10.79s  5.61%  strconv.atof32exact
    10.65s  5.53% 77.97%     10.65s  5.53%  aeshashbody
     6.76s  3.51% 81.49%     31.70s 16.47%  runtime.slicebytetostring
     4.56s  2.37% 83.85%      4.56s  2.37%  runtime.memmove
     3.73s  1.94% 85.79%     50.75s 26.36%  strconv.atof32

(pprof) list read.parseChunk
Total: 192.50s
ROUTINE ======================== wipm/brc/read.parseChunk in /home/paul/m/b/wipm-go/go-1brc/srv/read/main.go
    39.36s    189.14s (flat, cum) 98.25% of Total
         .          .     59:func parseChunk(chunk []byte, stations map[string][4]float64) error {
         .          .     60:
         .          .     61:   i := 0
         .          .     62:   for {
     110ms      110ms     63:           start_of_line := i
         .          .     64:
     5.92s      5.92s     65:           for ; i+1 < len(chunk) && chunk[i] != ';'; i++ {
         .          .     66:           }
     280ms      280ms     67:           if chunk[i] != ';' {
         .          .     68:                   return fmt.Errorf("Could not find reading delimiter")
         .          .     69:           }
     750ms     26.48s     70:           station_name := string(chunk[start_of_line:i])
         .          .     71:           i++
         .          .     72:
         .          .     73:           start_of_reading := i
    10.86s     10.86s     74:           for ; i < len(chunk) && chunk[i] != '\n'; i++ {
         .          .     75:           }
         .          .     76:           if chunk[i] != '\n' {
         .          .     77:                   return fmt.Errorf("Could not find end of line")
         .          .     78:           }
     3.81s     67.32s     79:           reading, err := strconv.ParseFloat(string(chunk[start_of_reading:i]), 32)
         .          .     80:           if err != nil {
         .          .     81:                   return fmt.Errorf("Invalid reading: %s", string(chunk[start_of_reading:i]))
         .          .     82:           }
     110ms      110ms     83:           i++
         .          .     84:
       11s     71.53s     85:           if station_readings, ok := stations[station_name]; !ok {
         .          .     86:                   initial_reading := [4]float64{reading, reading, reading, 1.0}
         .       10ms     87:                   stations[station_name] = initial_reading
         .          .     88:           } else {
     1.70s      1.70s     89:                   if station_readings[0] > reading {
     1.28s      1.28s     90:                           station_readings[0] = reading
     1.39s      1.39s     91:                   } else if station_readings[1] < reading {
     140ms      140ms     92:                           station_readings[1] = reading
         .          .     93:                   }
     1.25s      1.25s     94:                   station_readings[2] += reading
     650ms      650ms     95:                   station_readings[3]++
         .          .     96:           }
         .          .     97:
     110ms      110ms     98:           if i == len(chunk) {
         .          .     99:                   return nil
         .          .    100:           }
         .          .    101:   }
         .          .    102:}
         .          .    103:

::mem profile inspection

(pprof) top10
Showing nodes accounting for 21.88GB, 100% of 21.88GB total
Dropped 21 nodes (cum <= 0.11GB)
      flat  flat%   sum%        cum   cum%
   14.44GB 65.98% 65.98%    14.44GB 65.98%  wipm/brc/read.readChunk
    7.44GB 33.99%   100%     7.44GB 33.99%  wipm/brc/read.parseChunk
         0     0%   100%    14.44GB 65.98%  testing.(*B).run1.func1
         0     0%   100%    14.44GB 65.98%  testing.(*B).runN
         0     0%   100%    14.44GB 65.98%  wipm/brc/read.BenchmarkSolve
         0     0%   100%    14.44GB 65.98%  wipm/brc/read.Solve
         0     0%   100%     7.44GB 33.99%  wipm/brc/read.Solve.func2


## tag:v2.0 chunk reads

Instead of reading the file one line at at time it is in blocks of bytes (chunks). The aim is is to see if the IO is a significant factor of the processing time.

n. This is still a sequential implementation it just reads the a chunk of bytes that are then parsed rather than the file read line by line, the aim being to see if file IO is an issue.

Things to consider from this:

- Is there a way to make the standard reader used in v1 load bigger chunks
- There are still a lot of allocations
- This approach is sequential


:Notes
- parseChunk looks like a good candidate for optimization both cpu and memory allocations

:stats

BenchmarkSolve-16              1        181718007203 ns/op      23506915592 B/op  1000050465 allocs/op
ok      wipm/brc/read   181.857s


::cpu profile inspection: go tool pprof srv/read/read benchmarks/20240310114739.0e66530.cpu.prof

(pprof) top10
Showing nodes accounting for 146.34s, 84.67% of 172.83s total
Dropped 191 nodes (cum <= 0.86s)
Showing top 10 nodes out of 39
      flat  flat%   sum%        cum   cum%
    37.46s 21.67% 21.67%    168.33s 97.40%  wipm/brc/read.parseChunk
    29.89s 17.29% 38.97%     29.90s 17.30%  strconv.readFloat
    28.06s 16.24% 55.20%     51.34s 29.71%  runtime.mapaccess2_faststr
    11.36s  6.57% 61.78%     11.36s  6.57%  memeqbody
     9.87s  5.71% 67.49%     19.67s 11.38%  runtime.mallocgc
     9.13s  5.28% 72.77%      9.13s  5.28%  strconv.atof32exact
     6.86s  3.97% 76.74%      6.86s  3.97%  aeshashbody
     6.28s  3.63% 80.37%     28.51s 16.50%  runtime.slicebytetostring
     3.79s  2.19% 82.57%      3.79s  2.19%  runtime.memmove
     3.64s  2.11% 84.67%      3.64s  2.11%  runtime.nextFreeFast (inline)


(pprof) list read.parseChunk
Total: 172.83s
ROUTINE ======================== wipm/brc/read.parseChunk in /home/paul/m/b/wipm-go/go-1brc/srv/read/main.go
    37.46s    168.33s (flat, cum) 97.40% of Total
         .          .     59:func parseChunk(chunk []byte, stations map[string][4]float64) error {
         .          .     60:
         .          .     61:   i := 0
         .          .     62:   for {
     100ms      100ms     63:           start_of_line := i
         .          .     64:
     5.90s      5.90s     65:           for ; i+1 < len(chunk) && chunk[i] != ';'; i++ {
         .          .     66:           }
     190ms      190ms     67:           if chunk[i] != ';' {
         .          .     68:                   return fmt.Errorf("Could not find reading delimiter")
         .          .     69:           }
     660ms     24.23s     70:           station_name := string(chunk[start_of_line:i])
         .          .     71:           i++
         .          .     72:
         .          .     73:           start_of_reading := i
    10.57s     10.57s     74:           for ; i < len(chunk) && chunk[i] != '\n'; i++ {
         .          .     75:           }
         .          .     76:           if chunk[i] != '\n' {
         .          .     77:                   return fmt.Errorf("Could not find end of line")
         .          .     78:           }
     3.44s     59.40s     79:           reading, err := strconv.ParseFloat(string(chunk[start_of_reading:i]), 32)
         .          .     80:           if err != nil {
         .          .     81:                   return fmt.Errorf("Invalid reading: %s", string(chunk[start_of_reading:i]))
         .          .     82:           }
     130ms      130ms     83:           i++
         .          .     84:
    10.15s     61.49s     85:           if station_readings, ok := stations[station_name]; !ok {
         .          .     86:                   initial_reading := [4]float64{reading, reading, reading, 1.0}
         .          .     87:                   stations[station_name] = initial_reading
         .          .     88:           } else {
     1.64s      1.64s     89:                   if station_readings[0] > reading {
     1.21s      1.21s     90:                           station_readings[0] = reading
     1.49s      1.49s     91:                   } else if station_readings[1] < reading {
     180ms      180ms     92:                           station_readings[1] = reading
         .          .     93:                   }
     1.07s      1.07s     94:                   station_readings[2] += reading
     690ms      690ms     95:                   station_readings[3]++
         .          .     96:           }
         .          .     97:
      40ms       40ms     98:           if i == len(chunk) {
         .          .     99:                   return nil
         .          .    100:           }
         .          .    101:   }
         .          .    102:}

::mem profile inspection: go tool pprof srv/read/read benchmarks/20240310114739.0e66530.mem.prof
(pprof) top
Showing nodes accounting for 22490.06MB, 100% of 22494.79MB total
Dropped 15 nodes (cum <= 112.47MB)
      flat  flat%   sum%        cum   cum%
14785.80MB 65.73% 65.73% 14785.80MB 65.73%  wipm/brc/read.readChunk
 7703.26MB 34.24%   100%  7703.26MB 34.24%  wipm/brc/read.parseChunk
       1MB 0.0044%   100% 22490.06MB   100%  wipm/brc/read.Solve
         0     0%   100% 22490.06MB   100%  testing.(*B).run1.func1
         0     0%   100% 22490.06MB   100%  testing.(*B).runN
         0     0%   100% 22490.06MB   100%  wipm/brc/read.BenchmarkSolve



## tag:v1 Basic implementation

As per the reference article this is just a basic implementation that reads the file one line at a time, parses it, updates the statistics for the station the reading is for and once all lines have been read outputs the statistics. 

There are some differences to what is specified in the challenge:

- All station names are the same size
- The stations are not being sorted before output

:stats

BenchmarkSolve-16              1        259803323898 ns/op      48003246472 B/op  2000050885 allocs/op
ok      wipm/brc/read   260.013s
