# Billion Row Challenge

Experiments in loading a billion rows from a file and seeing how efficient that can be made. Note a lot of the intelectual property right goes to the blog post, for me this was more around playing with the ideas presenten in there to get a better understanding of how go works.


## Binaries

There are the following projects, located within the srv directory:

- generate  ,allows generation of a data file 
- read      ,reads data from the file


### Read

Process that reads the data from the file, will be used for experimenting on what effect different approaches have.

command line arguments:

- data  ,path to the data file that it expects to load


### Generate

Simple cli tool that allows specifying the number of rows to generate and the number of stations that you want and then generates the data for that and outputs it to the stdout.

command line arguments:

-rows      ,number of rows you want to generate
-stations  ,number of stations that the measure should be distirbuted across

station names are uniform in size and randomly generated garbage strings


## Tools

- bm.sh, runs a benchmark aginst the latest version of the read code 
- pf.sh, allows you select a profile file and then opens it in go tool pprof


## References

- https://www.bytesizego.com/blog/one-billion-row-challenge-go
- https://www.youtube.com/watch?v=cYng524S-MA
