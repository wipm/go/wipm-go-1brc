#!/usr/bin/env bash

script_dir=$(realpath $(dirname ${BASH_SOURCE[0]}))


# setup benchmark directory for storing profile results
benchmark_dir=$script_dir/benchmarks
if [ ! -d $benchmark_dir ]; then
    mkdir $benchmark_dir
fi


# Get datafile to use in benchmark
while getopts ":f:w:" opt; do
    case $opt in
        f) 
            file=$OPTARG
            ;;
        w)
            parsers=$OPTARG
            ;;
    esac
done

if [ -z $parsers ]; then
    parsers="4"
fi

if [ -z $file ]; then
    echo "Usage: $0 -f <file>"
    exit 1
fi
file=$(realpath $file)


# Run benchmarks 
profile_id="$(date +"%Y%m%d%H%M%S").$(git rev-parse --short HEAD)"
cd $script_dir/srv/read
DATA_FILE=$file NUM_CHUNK_PARSERS=$parsers go test -cpuprofile $benchmark_dir/${profile_id}.cpu.prof -memprofile $benchmark_dir/${profile_id}.mem.prof -bench . -benchmem

