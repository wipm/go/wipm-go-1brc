#!/usr/bin/env bash

if ! command -v fzf >/dev/null 2>&1; then
  echo "fzf not installed.  Aborting."
  exit 1
fi

profile_file=$(find ./benchmarks -type f | fzf)
go tool pprof srv/read/read "$profile_file"

