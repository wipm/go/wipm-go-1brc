package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
	"sync"
)

// Reads a chunk of up to chunkSize bytes from the file. The returned chunk will
// be aligned to the last newline character, all lines in the returned bytes will
// be terminated with a newline of.
//
// If the last line in the file was not terminated with a new line character one
// will be appended.  If the chunk needed to be truncated to terminate with a
// newline character the files position will be updates to the next byte after
// that position.
func readChunk(file *os.File, fileSize int64, chunkSizeInBytes int) ([]byte, error) {
	buf := make([]byte, chunkSizeInBytes+1)

	n, err := file.Read(buf[:chunkSizeInBytes])
	if err != nil {
		if err == io.EOF {
			return buf[:n], nil
		}
		return nil, err
	}

	file_position, err := file.Seek(0, io.SeekCurrent)
	if err != nil {
		return nil, err
	}

	if file_position == fileSize {
		if buf[n-1] != '\n' {
			buf[n] = '\n'
			return buf[:n+1], nil
		}
	}

	for i := n - 1; i >= 0; i-- {
		if buf[i] == '\n' {
			overshoot := n - i - 1
			if overshoot > 0 {
				_, err := file.Seek(int64(-overshoot), io.SeekCurrent)
				if err != nil {
					return nil, err
				}
			}
			return buf[:i+1], nil
		}
	}
	return nil, fmt.Errorf("Could not align chunk to end of line, no newline found")

}

func parseChunk(chunk []byte, stations map[string][4]float64) error {

	i := 0
	for {
		start_of_line := i

		for ; i+1 < len(chunk) && chunk[i] != ';'; i++ {
		}
		if chunk[i] != ';' {
			return fmt.Errorf("Could not find reading delimiter")
		}
		station_name := string(chunk[start_of_line:i])
		i++

		start_of_reading := i
		for ; i < len(chunk) && chunk[i] != '\n'; i++ {
		}
		if chunk[i] != '\n' {
			return fmt.Errorf("Could not find end of line")
		}
		reading, err := strconv.ParseFloat(string(chunk[start_of_reading:i]), 32)
		if err != nil {
			return fmt.Errorf("Invalid reading: %s", string(chunk[start_of_reading:i]))
		}
		i++

		if station_readings, ok := stations[station_name]; !ok {
			initial_reading := [4]float64{reading, reading, reading, 1.0}
			stations[station_name] = initial_reading
		} else {
			if station_readings[0] > reading {
				station_readings[0] = reading
			} else if station_readings[1] < reading {
				station_readings[1] = reading
			}
			station_readings[2] += reading
			station_readings[3]++
		}

		if i == len(chunk) {
			return nil
		}
	}
}

func aggregateStats(chunks_stats map[string][4]float64, aggregated_stats map[string][4]float64) {

	for key, value := range chunks_stats {

		if aggregated_stat, ok := aggregated_stats[key]; !ok {
			aggregated_stats[key] = value
		} else {
			if aggregated_stat[0] > value[0] {
				aggregated_stat[0] = value[0]
			}
			if aggregated_stat[1] < value[1] {
				aggregated_stat[1] = value[1]
			}
			aggregated_stat[2] += value[2]
			aggregated_stat[3] += value[3]
		}
	}
}

func Solve(data_file string, num_chunk_parsers int) {

	file, err := os.Open(data_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error opening file: %s\n", err)
		os.Exit(1)
	}
	defer file.Close()

	file_info, err := file.Stat()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error getting file info: %s\n", err)
		os.Exit(1)
	}
	file_size := file_info.Size()

	/*
	   +-----------+                                  +--------------+                               +-----------+
	   ReadChunks  |  chunk -> chan:raw_chunks     <- (*)ParseChunks | -> chan:chunks_stats       <- AggregateStat -> chan:stats
	   +-----------+                                  +--------------+                               +-----------+

	   (*) - multiple instances of ParseChunks
	*/

	raw_chunks := make(chan []byte, num_chunk_parsers*5)
	chunks_stats := make(chan map[string][4]float64, num_chunk_parsers*2)
	stats := make(chan map[string][4]float64)

	// t. Error handling

	// aggregate stats routine
	go func() {
		stations := make(map[string][4]float64)

		for partial_stats := range chunks_stats {
			aggregateStats(partial_stats, stations)
		}
		stats <- stations
	}()

	// parse chunks routines
	var chunks_stats_done sync.WaitGroup
	chunks_stats_done.Add(num_chunk_parsers)

	for i := 0; i < num_chunk_parsers; i++ {

		go func() {
			defer chunks_stats_done.Done()
			stations := make(map[string][4]float64)
			for chunk := range raw_chunks {
				// t. error handling
				_ = parseChunk(chunk, stations)
			}
			chunks_stats <- stations
		}()
	}

	go func() {
		chunks_stats_done.Wait()
		close(chunks_stats)
	}()

	// read chunks
	for {
		chunk, err := readChunk(file, file_size, 1024*1024*64)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading chunk: %s\n", err)
			os.Exit(1)
		}
		if len(chunk) == 0 {
			close(raw_chunks)
			break
		}
		raw_chunks <- chunk
	}

	// t. need to listen for process signal
	stations := <-stats

	for k, v := range stations {
		mean := v[2] / v[3]
		fmt.Printf("%s:%f/%f/%f;", k, v[0], mean, v[1])
	}
}

func main() {
	data_file := flag.String("data", "", "data file path")
	num_chunk_parsers := flag.Int("chunk-parsers", 4, "Number of chunk parsers")
	flag.Parse()

	if *data_file == "" {
		fmt.Fprintf(os.Stderr, "Need to specify the data file\n")
		flag.Usage()
		os.Exit(1)
	}

	Solve(*data_file, *num_chunk_parsers)
}
