package main

import (
	"os"
	"strconv"
	"testing"
)

func BenchmarkSolve(b *testing.B) {
	data_file := os.Getenv("DATA_FILE")
	num_chunk_parsers, err := strconv.Atoi(os.Getenv("NUM_CHUNK_PARSERS"))
	if err != nil {
		os.Exit(1)
	}
	os.Stdout, _ = os.Open(os.DevNull)

	for i := 0; i < b.N; i++ {
		Solve(data_file, num_chunk_parsers)
	}
}
