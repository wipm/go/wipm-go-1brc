package main


import (
    "flag"
    "fmt"
    "math/rand"
    "os"
)

var alphabet = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randString(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = alphabet[rand.Intn(len(alphabet))]
    }
    return string(b)
}

func randFloat(min, max float64) float64 {
    // rand.Float64() * (max - min) 
    //
    // scales the value to the range [0, max-min]
    // ensure the value will always be in the range when
    // added to the min value and ensures an even distribution
    // within the range
    return min + rand.Float64() * (max - min)
}

func generateStations(n int) []string {
    stationsSet := make(map[string]bool)

    stations := make([]string, n)
    for i:=0; i < n; i++ {
        for {
            station := randString(8)
            if _, ok := stationsSet[station]; !ok {
                stationsSet[station] = true
                stations[i] = station
                break
            }
        }
    }
    return stations
}

func main() {
    num_rows := flag.Int("rows", 0, "number of rows")
    num_stations := flag.Int("stations", 0, "number of stations")
    flag.Parse()

    if *num_rows <= 0 {
        println("Need to specify the number of rows")
        flag.Usage()
        os.Exit(1)
    }

    if *num_stations <= 0 {
        println("Need to specify the number of stations")
        flag.Usage()
        os.Exit(1)
    }


    stations := generateStations(*num_stations)

    for i:=0; i < *num_rows; i++ {
        station := stations[rand.Intn(len(stations))]
        measure := randFloat(-99.99, 99.99)
        fmt.Printf("%s;%.2f\n", station, measure)
    }
}
