# Readchunks

Experiment to read fixed size chunks from a file and then align the buffer to the end of the last full line.  Reading in chunks is fairly straigth forward as it is just a standard libary call, aligning to the end of the last line has some edge cases around if the last line in the file was terminated with a newline character.  If it was not then a new line character is appended to the end of the buffer.

This approach only works if you know that the chunk size (max number of bytes read in an single read) is larger than a line if the chunk size is smaller than a line then the process will terminate with an error.

line size  :  |--------------------------\n|-------...
chunk size :  |----------------|                         // Undersized - Will error as it can not find the end of the line
              |----------------------------|             // Aligned    - Same size last character read is a new line
              |--------------------------------|         // Oversizes  - Will fallback to the last new line buffer and file position


## Testing

The code was written without tests but there are some test cases that can be run. The test cases require data file generation, test.md contains a descript of the test cases along with the command that needs to be run to exercise the test and a set of command for generating the test data files.

The test cases pass if the data file that the test case runs againt is run to completion.

There is also an alias files alias.sh once sources will introduce an alias tc  that will allow you to select the testcase or data generation you want from the list defined in test.md and run it.

n. The alias depends on fzf being installed

