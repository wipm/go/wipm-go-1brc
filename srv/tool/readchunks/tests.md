# Test cases (n. Generate data first ) ; :

test line size aligned, one line                 ;./readchunks -chunk 11 -data ./10.3.rows
test line size aligned, two lines                ;./readchunks -chunk 22 -data ./10.3.rows
test overflow, one character                     ;./readchunks -chunk 12 -data ./10.3.rows
test overflow, two characters                    ;./readchunks -chunk 13 -data ./10.3.rows
test aligned, last line not terminated with \n   ;./readchunks -chunk 11 -data ./10.3.x1.rows
test overflow, last line not terminated with \n  ;./readchunks -chunk 13 -data ./10.3.x1.rows


# Generate Data (lines will have l+1, number of characters (l) + \n) ; : 

data generate 10.3.rows data file     ;rm -f 10.3.rows && ../uns.sh -n 3 -l 10 -p ":" > 10.3.rows
data generate 10.3.x1.rows data file  ;rm -f 10.3.x1.rows && ../uns.sh -n 3 -l 10 -p "#" > 10.3.x1.data && cat 10.3.x1.data | head -c -1 > 10.3.x1.rows && rm 10.3.x1.data
data delete test data                 ;rm -f ./*.rows

# build
build exe                        ;rm -f ./readchunks && go build .

