package main

import (
	"flag"
	"fmt"
	"io"
	"os"
)

func readChunk(file *os.File, fileSize int64, chunkSize int) ([]byte, error) {
	buf := make([]byte, chunkSize+1)

    n, err := file.Read(buf[:chunkSize])
	if err != nil {
        if err == io.EOF {
            return buf[:0], nil
        }
		return nil, err
	}


    fmt.Println("read: %d",n)

    // deal with edge case where the last line in the file
    // does not terminate with a newline 
    currentPosition, err := file.Seek(0,io.SeekCurrent)
    if err != nil {
        return nil, err
    }

    if currentPosition == fileSize {
        fmt.Println("at end of file")

        if buf[n-1] != '\n' {
            fmt.Println("Append \\n ")
            buf[n] = '\n'
            n++
        }
    }

    // align buffer to last full line
    for i := n-1; i > -1; i-- {
        if buf[i] == '\n' {
            overshoot := n-1-i
            if overshoot > 0 {
                _,err = file.Seek(int64(-overshoot),io.SeekCurrent)
                if err != nil {
                    return nil, err
                }
            }
            return buf[:i+1], nil
        }
    }
	return nil, fmt.Errorf("Could not align buffer to end of file")
}

func main() {
	data_file := flag.String("data", "./test.data", "Data file")
	chunk_size := flag.Int("chunk", 10, "Chunk size in bytes")
	flag.Parse()

	file, err := os.Open(*data_file)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	defer file.Close()

    fileStats, err := file.Stat()
    if err != nil {
        fmt.Println("Error", err)
        os.Exit(1)
    }

	for {
        chunk, err := readChunk(file, fileStats.Size(), *chunk_size)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading chunk\n")
            fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			os.Exit(1)
		}

		if len(chunk) == 0 {
			return
		}

		fmt.Fprintf(os.Stdout, "-- start of chunk\n")
		fmt.Fprintf(os.Stdout, "[%s]",string(chunk))
		fmt.Fprintf(os.Stdout, "-- end of chunk\n")
	}
}
