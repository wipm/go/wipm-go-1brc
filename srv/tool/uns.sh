#!/usr/bin/env bash

usage() {
    echo "Usage: uns -n <number of lines> -l <length of each line> [-p <prefix>]"
}

while getopts ":n:p:l:" opt; do
    case $opt in
        n) num=$OPTARG ;;
        p) prefix=$OPTARG ;;
        l) length=$OPTARG ;;
        *) echo "Invalid option: -$OPTARG" ;;
    esac
done

if [ -z $num ]; then
    usage 
    exit 1
fi

if [ -z $length ]; then
    length=10
fi


seq 1 $num | cat | xargs -I {} openssl rand -base64 48 | sed "s/^/$prefix/" | cut -c -$length
